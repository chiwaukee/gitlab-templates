# Chiwaukee Gitlab Templates

## Usage

1. Add the following variables to your `.gitlab-ci.yml`:

```yml
variables:
  REPO_LOCATION: # full path to the in artifact registry, i.e., us-east1-docker.pkg.dev/what-am-i-listening-to-357216/what-am-i-listening-to
  IMAGE_NAME: # name of the docker image in artifact registry, i.e., whatamilisteningto
  SERVICE_NAME: # name of the service in cloud run, i.e., whatsreidlisteningto
  PROJECT_ID: # id of the project used, i.e., what-am-i-listening-to-357216
  REGION: # i.e., us-east1
```

2. Have a `GCLOUD_SERVICE_KEY_JSON` secret variable defined in the project CI/CD settings

3. Have a `Build` and `Release` stage defined in your `.gitlab-ci.yml`
